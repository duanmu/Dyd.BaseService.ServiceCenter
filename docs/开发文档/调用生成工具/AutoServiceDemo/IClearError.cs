
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoServiceDemo
{
    /// <summary>
    /// 【服务】AutoServiceDemo,【描述】AutoServiceDemo
    /// </summary>
    public class IClearErrorClient:XXF.BaseService.ServiceCenter.Client.RemoteClient.ThriftRemoteClient
    {
        public IClearErrorClient()
        {
        }

        
        /// <summary>
        /// 【方法】AutoServiceDemo,【描述】AutoServiceDemo
        /// </summary>
           
        public List<OrderMain> ClearOne()
        {
            return Call<List<OrderMain>>("ClearOne",new object[]{}); 
        }

       
    }
}
