﻿using Dyd.BaseService.ServiceCenter.Domain;
using Dyd.BaseService.ServiceCenter.Domain.Bll;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Web.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {

        #region List
        /// <summary>
        /// 
        /// </summary>
        /// <param name="search">查询实体</param>
        /// <param name="pno">页码</param>
        /// <param name="pagesize">页大小</param>
        /// <returns></returns>
        public ActionResult ClientIndex(tb_client_search search)
        {
            ViewBag.ServerId = search.serverid;
            ViewBag.PageSize = search.PageSize;
            int total = 0;
            IList<tb_client> list = new List<tb_client>();
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                list = tb_client_bll.Instance.GetPageList(conn, search, out total);
                var pagelist = new SPagedList<tb_client>(list, search.Pno, search.PageSize, total);

                if (Request.IsAjaxRequest())
                {
                    return PartialView("_ClientIndex", pagelist);
                }
                else
                {
                    return View(pagelist);
                }
            }
        }
        #endregion

        #region Create
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult ClientCreate()
        {
            return View();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult tb_clientCreate(tb_client model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    if (tb_client_bll.Instance.IsExists(conn, model))
                    {
                        return Json(new { Flag = false, Message = "！" });
                    }
                    tb_client_bll.Instance.Add(conn, model);
                    return Json(new { Flag = true, Message = "添加成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion

        #region Edit

        /// <summary>
        /// 修改配置
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        public ActionResult ClientEdit(int id)
        {
            using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
            {
                conn.Open();
                var model = tb_client_bll.Instance.Get(conn, id);
                return View(model);
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model">实体</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReturnConfigEdit(tb_client model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    if (tb_client_bll.Instance.IsExists(conn, model))
                    {
                        return Json(new { Flag = false, Message = "！" });
                    }
                    tb_client_bll.Instance.Update(conn, model);
                    return Json(new { Flag = true, Message = "！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }

        #endregion

        #region Delete
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientDelete(int id)
        {
            try
            {
                using (DbConn conn = DbConfig.CreateConn(DataConfig.ServiceCenterConnectString))
                {
                    conn.Open();
                    tb_client_bll.Instance.Delete(conn, id);
                    return Json(new { Flag = true, Message = "删除成功！" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Flag = false, Message = ex.Message });

            }
        }
        #endregion
    }
}