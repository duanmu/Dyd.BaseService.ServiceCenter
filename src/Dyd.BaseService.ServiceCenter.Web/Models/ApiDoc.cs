﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dyd.BaseService.ServiceCenter.Web.Models
{
    public class ApiDoc
    {
        public ApiDoc()
        {
            ServiceDocList = new List<ServiceDoc>();
        }
        public List<ServiceDoc> ServiceDocList { get; set; }
    }
}