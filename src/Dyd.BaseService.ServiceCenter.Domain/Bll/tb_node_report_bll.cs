﻿using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using XXF.Cache;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Domain.Bll
{
    public partial class tb_node_report_bll
    {
        #region Init
        public static tb_node_report_bll Instance = new tb_node_report_bll();
        private readonly Maticsoft.DAL.tb_node_report_dal dal = new Maticsoft.DAL.tb_node_report_dal();
        private tb_node_report_bll()
        { }
        #endregion

        #region C
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_node_report_model model)
        {
            return dal.Add(conn, model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_node_report_model model)
        {
            return dal.Update(conn, model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            return dal.Delete(conn, id);
        }
        #endregion

        #region Q
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool IsExists(DbConn conn, tb_node_report_model model)
        {
            return dal.IsExists(conn, model);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_node_report_model Get(DbConn conn, int id)
        {
            return dal.Get(conn, id);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public tb_node_report_model GetByCache(DbConn conn, int id)
        {
            string CacheKey = "tb_node_report_Model-" + id;
            var model = CacheHelper.GetCache(CacheKey + id, TimeSpan.FromMinutes(1), () => dal.Get(conn, id));
            return (tb_node_report_model)model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<tb_node_report_model> GetPageList(DbConn conn, tb_node_report_model_search search, out int totalCount)
        {
            return dal.GetPageList(conn, search, out  totalCount);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<tb_node_report_model> GetList(DbConn conn, tb_node_report_model_search search)
        {
            return dal.GetList(conn, search);
        }
        #endregion

    }
}