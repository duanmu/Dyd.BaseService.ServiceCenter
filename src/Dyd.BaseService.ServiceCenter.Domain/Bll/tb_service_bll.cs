﻿using Dyd.BaseService.ServiceCenter.Domain.Dal;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using XXF.Cache;
using XXF.Db;
namespace Dyd.BaseService.ServiceCenter.Domain.Bll
{
    //tb_service
    public partial class tb_service_bll
    {
        #region Init
        public static tb_service_bll Instance = new tb_service_bll();
        private readonly tb_service_dal dal = new tb_service_dal();
        private readonly tb_node_dal nodeDal = new tb_node_dal();
        private readonly tb_protocolversion_dal protocolversionDal = new tb_protocolversion_dal();
        private tb_service_bll()
        { }
        #endregion

        #region C
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_service model)
        {
            return dal.Add(conn, model);
        }

        /// <summary>
        /// 设置服务协议
        /// </summary>
        public int SetDefaultProtocolVersion(DbConn conn, int nodeId)
        {
            conn.BeginTransaction();
            try
            {
                var node = nodeDal.Get(conn, nodeId);
                if (null != node)
                {
                    tb_service serviceModel = dal.Get(conn, node.serviceid);
                    serviceModel.protocolversion++;
                    tb_protocolversion model = new tb_protocolversion
                    {
                        createtime = DateTime.Now,
                        interfaceversion = node.interfaceversion,
                        protocoljson = node.protocoljson,
                        serviceid = node.serviceid,
                        version = serviceModel.protocolversion,
                    };
                    dal.Update(conn, serviceModel);
                    var ret = protocolversionDal.Add(conn, model);
                    conn.Commit();
                    return ret;
                }
                return 0;
            }
            catch
            {
                conn.Rollback();
                throw;
            }
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_service serviceModel, tb_protocolversion protocolVersionModel)
        {
            conn.BeginTransaction();
            try
            {
                var serviceId = dal.Add(conn, serviceModel);
                protocolVersionModel.serviceid = serviceId;
                protocolversionDal.Add(conn, protocolVersionModel);
                conn.Commit();
                return serviceId;
            }
            catch
            {
                conn.Rollback();
                throw;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_service model)
        {
            return dal.Update(conn, model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            return dal.Delete(conn, id);
        }
        #endregion

        #region Q
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool IsExists(DbConn conn, tb_service model)
        {
            return dal.IsExists(conn, model);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_service Get(DbConn conn, int id)
        {
            return dal.Get(conn, id);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public tb_service GetByCache(DbConn conn, int id)
        {
            string CacheKey = "tb_serviceModel-" + id;
            var model = CacheHelper.GetCache(CacheKey + id, TimeSpan.FromMinutes(1), () => dal.Get(conn, id));
            return (tb_service)model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<tb_service> GetPageList(DbConn conn, tb_service_search search, out int totalCount)
        {
            var list = dal.GetPageList(conn, search, out  totalCount);
            foreach (var item in list)
            {
                item.runningnodecount = nodeDal.GetNodeCount(conn, item.id, XXF.BaseService.ServiceCenter.SystemRuntime.EnumRunState.Running);
                item.stopnodecount = nodeDal.GetNodeCount(conn, item.id, XXF.BaseService.ServiceCenter.SystemRuntime.EnumRunState.Stop);
            }
            return list;
        }
        #endregion

    }
}