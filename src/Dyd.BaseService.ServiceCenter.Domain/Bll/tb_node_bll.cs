﻿using Dyd.BaseService.ServiceCenter.Domain.Dal;
using Dyd.BaseService.ServiceCenter.Domain.Model;
using System;
using System.Collections.Generic;
using XXF.Cache;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Domain.Bll
{
    //tb_node
    public partial class tb_node_bll
    {
        #region Init
        public static tb_node_bll Instance = new tb_node_bll();
        private readonly tb_node_dal dal = new tb_node_dal();
        private tb_node_bll()
        { }
        #endregion

        #region C
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(DbConn conn, tb_node model)
        {
            return dal.Add(conn, model);

        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(DbConn conn, tb_node model)
        {
            return dal.Update(conn, model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(DbConn conn, int id)
        {
            return dal.Delete(conn, id);
        }


        /// <summary>
        /// 更新节点权重值
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="id">Id</param>
        /// <param name="boostPercent">权重值</param>
        /// <returns></returns>
        public bool UpdateNodeBoostPercent(DbConn conn, int id, int boostPercent)
        {
            return dal.UpdateNodeBoostPercent(conn, id, boostPercent);
        }
        #endregion

        #region Q
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool IsExists(DbConn conn, tb_node model)
        {
            return dal.IsExists(conn, model);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public tb_node Get(DbConn conn, int id)
        {
            return dal.Get(conn, id);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        /// <param name="timeOut">超时时间（毫秒）</param>
        public IList<tb_node> GetTimeOutNode(DbConn conn, int timeOut)
        {
            return dal.GetTimeOutNode(conn, timeOut);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public tb_node GetByCache(DbConn conn, int id)
        {
            string CacheKey = "tb_nodeModel-" + id;
            var model = CacheHelper.GetCache(CacheKey + id, TimeSpan.FromMinutes(1), () => dal.Get(conn, id));
            return (tb_node)model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public IList<tb_node> GetPageList(DbConn conn, tb_node_search search, out int totalCount)
        {
            return dal.GetPageList(conn, search, out  totalCount);
        }
        #endregion

    }
}